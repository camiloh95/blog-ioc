
# GKE cluster
resource "google_container_cluster" "jenkins-primary" {
  provider = google
  name     = "jenkins-gke"
  location = var.google_region

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name

  ip_allocation_policy {}

  enable_autopilot = true
}