terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "camilohernandez"

    workspaces {
      name = "development"
    }
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.4.0"
    }
    google = {
      source = "hashicorp/google"
      version = "4.13.0"
    }
  }

  required_version = "~> 1.1.6"
}

provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

provider "google" {
  credentials = var.GOOGLE_CREDENTIALS
  project = "camilohernandez-blog"
  region  = var.google_region
  zone    = var.google_default_zone
}

provider "google-beta" {
  credentials = var.GOOGLE_CREDENTIALS
  project = "camilohernandez-blog"
  region  = var.google_region
  zone    = var.google_default_zone
}