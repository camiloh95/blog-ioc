resource "google_artifact_registry_repository" "blog-api-repository" {
  provider = google-beta
  location = var.google_region
  repository_id = "blog-api"
  description = "repository to store API blog images"
  format = "DOCKER"
}