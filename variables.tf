variable "aws_region" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "GOOGLE_CREDENTIALS" {}
variable "project_id" {}
variable "google_region" {}
variable "google_default_zone" {}